import django
import os

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'core.settings') # this should be done first.

# Import settings
django.setup() # This needs to be done after you set the environ

from unittest.mock import patch

from django.test import TestCase 
from django.contrib.auth import get_user_model

from app.models import Student, Teacher, image_file_path


def sample_user(email='test@recipeapi.com', password='@Testpass'):
    """ Criar um usuário teste """
    return get_user_model().objects.create_user(email, password)


class ModelTests(TestCase):
    def test_create_user_with_email(self):
        """ Teste: Criar novo usuário com o email """
        email = 'test@apirecipe.com'
        password = '@Test123'
        user = get_user_model().objects.create_user(
            email=email,
            password=password
        )

        self.assertEqual(user.email, email)
        self.assertTrue(user.check_password(password))

    def test_new_user_email_normalized(self):
        """ Teste: Corrige distorções do email """

        email = 'testrecepeapi@GMAIL.COM'
        user = get_user_model().objects.create_user(email, 'test123')
        # print(user.email)

        self.assertEqual(user.email, email.lower())

    def test_new_user_email_invalid(self):
        """ Teste: Verificar se foi colocado algum email """
        with self.assertRaises(ValueError):
            get_user_model().objects.create_user(None, 'teste123456')

    def test_create_new_superuser(self):
        """ Teste: Criar novo super usuário """
        user = get_user_model().objects.create_superuser(
            'test@gmail.com',
            'test@123'
        )

        self.assertTrue(user.is_superuser)
        self.assertTrue(user.is_staff)

    def test_create_student_str(self):
        """ Teste: Criar representação string de estudantes """
        student = Student.objects.create(
            user=sample_user(),
            name='Pedro',
            register=20180504
        )

        self.assertEqual(str(student), student.name)

    def test_create_teacher_str(self):
        """ Teste: Criar representação de string de professores """
        teacher = Teacher.objects.create(
            user=sample_user(),
            name='Tonhãum'
        )

        self.assertEqual(str(teacher), teacher.name)

    @patch('uuid.uuid4')
    def test_name_uuid(self, mock_uuid):
        """ Teste: Salvar imagem no diretório correto.  """
        uuid = 'test_uuid'
        mock_uuid.return_value = uuid

        file_path = image_file_path(None, 'myimage.jpg')
        ext_path = f'uploads/profiles/{uuid}.jpg'

        self.assertEqual(file_path, ext_path)
