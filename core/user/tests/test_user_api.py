import django
import os

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'core.settings') # this should be done first.

# Import settings
django.setup() # This needs to be done after you set the environ


from django.test import TestCase
from django.contrib.auth import get_user_model
from django.urls import reverse

from rest_framework.test import APIClient
from rest_framework import status

CREATE_USER_URL = reverse('user:create')
TOKEN_URL = reverse('user:token')
ME_URL = reverse('user:me')


def create_user(**params):
    """ Criar novo usuário """
    return get_user_model().objects.create_user(**params)


class PublicApiTests(TestCase):
    """ Teste: Usuários (publica) """

    def setUp(self):
        self.client = APIClient()

    def test_create_valid_user_success(self):
        """ Teste: Criando novo usuário com payload """

        payload = {
            'email': 'peidrao01@gmail.com',
            'password': '@testpass123',
            'name': 'Pedro'
        }

        res = self.client.post(CREATE_USER_URL, payload)

        self.assertEqual(res.status_code, status.HTTP_201_CREATED)
        user = get_user_model().objects.get(**res.data)

        self.assertTrue(user.check_password(payload['password']))
        self.assertNotIn('password', res.data)

    def test_user_exists(self):
        """ Teste: Verificar se o usuário a ser criado já existe """

        payload = {
            'email': 'peidrao02@gmail.com',
            'password': '@test123'
        }

        create_user(**payload)

        res = self.client.post(CREATE_USER_URL, payload)
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)

    def test_password_too_short(self):
        """ Teste: Verificar se a senha possui mais de 5 caracteres """

        payload = {
            'email': 'peidrao011231@gmail.com',
            'password': '1234'
        }

        res = self.client.post(CREATE_USER_URL, payload)
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)

        user_exists = get_user_model().objects.filter(
            email=payload['email']
            ).exists()

        self.assertFalse(user_exists)

    """ Testes relacionados ao Token do usuário """

    def test_create_for_user(self):
        """ Teste: Verificar se token foi criado para usuário """
        payload = {
            'email': 'peidrao123@gmail.com',
            'password': 'test123456'
        }
        create_user(**payload)
        res = self.client.post(TOKEN_URL, payload)

        self.assertIn('token', res.data)
        self.assertEqual(res.status_code, status.HTTP_200_OK)

    def test_create_token_invalid(self):
        """ Teste: Verificar se token foi criado """
        create_user(email='pedro@gmail.com', password='@Teste12312221')
        payload = { 'email':'pedro@gmail.com', 'password':'@wrong'}
        
        res = self.client.post(TOKEN_URL, payload)

        self.assertNotIn('token', res.data)
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
    
    def test_create_token_no_user(self):
        """ Teste: Verificar se token não foi criado existir se o usuário não foi criado """
        payload = {'email': 'pedro@gmail.com', 'password': '@wrong123'}
        res = self.client.post(TOKEN_URL, payload)

        self.assertNotIn('token', res.data)
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)

    def test_create_token_missing_field(self):
        """ Teste: Email e senha são requeridos """

        res = self.client.post(TOKEN_URL, { 
            'email': 'one',
            'password': ''
        })

        self.assertNotIn('token', res.data)
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)

    def test_retrieve_user_unauthorized(self):
        """ Teste: Autênticação é requirida """ 
        res = self.client.get(ME_URL)

        self.assertEqual(res.status_code, status.HTTP_401_UNAUTHORIZED)


class PrivateUserApiTest(TestCase):
    
    def setUp(self):
        self.user = create_user(
            email='peidrao@pedro.com.br',
            password='@test12312',
            name='Pedro'
        )

        self.client = APIClient()
        self.client.force_authenticate(user=self.user)

    def test_retrieve_profile_success(self):
        """ Test retrieving profile for logged in used """
        res = self.client.get(ME_URL)

        self.assertEqual(res.status_code, status.HTTP_200_OK)
        self.assertEqual(res.data, {'name': self.user.name, 'email': self.user.email})
    
    def test_post_me_not_allowed(self):
        """  Test that POST is not allowed on the me url"""
        res = self.client.post(ME_URL, {})
        self.assertEqual(res.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_update_user_profile(self):
        """ Test updating the user profile for authenticated user """

        payload = {'name': 'new name', 'password': 'newpassword!'}
        res = self.client.patch(ME_URL, payload)

        self.user.refresh_from_db()
        self.assertEqual(self.user.name, payload['name'])
        self.assertFalse(self.user.check_password(payload['password']))
        self.assertEqual(res.status_code, status.HTTP_200_OK)