# from rest_framework.decorators import action
from rest_framework import viewsets, mixins
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated

from app.models import Student, Teacher, ClassRoom
from .serializers import StudentSerializer, TeacherSerializer, ClassRoomSerializer, ClassRomDetailSerializer


class BaseClassRoomAttrViewSet(viewsets.GenericViewSet,
                               mixins.ListModelMixin,
                               mixins.CreateModelMixin):
    """ Base viewset for user owned classroom attributes """

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        return self.queryset.filter(user=self.request.user).order_by('-name')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class StudentViewSet(BaseClassRoomAttrViewSet):
    """ Manage student's in the database """
    queryset = Student.objects.all()
    serializer_class = StudentSerializer


class TeacherViewSet(BaseClassRoomAttrViewSet):
    """ Manage teacher's in the database """
    queryset = Teacher.objects.all()
    serializer_class = TeacherSerializer


class ClassRoomViewSet(viewsets.ModelViewSet):
    """ Manage classroom in the database """

    serializer_class = ClassRoomSerializer
    queryset = ClassRoom.objects.all()
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def params_to_ints(self, qs):
        """ Convert a list of string IDs to a list of intengers """
        return [int(str_id) for str_id in qs.split(',')]
    
    def get_queryset(self):
        """ Retrieve the recipes for the authenticated user """
        students = self.request.query_params.get('students')
        teacher = self.request.query_params.get('teachers')
        queryset = self.queryset

        if students:
            students_ids = self.params_to_ints(students)
            queryset = queryset.filter(students__id__in=students_ids)
        if teacher:
            teacher_ids = self.params_to_ints(teacher)
            queryset = queryset.filter(teacher__id__in=teacher_ids)

        return queryset.filter(user=self.request.user)
    
    def get_serializer_class(self):
        """ Return appropriate serializer class """
        if self.action == 'retrieve':
            return ClassRomDetailSerializer
        
        return self.serializer_class

    def perform_create(self, serializer):
        """ Create a new classroom """
        serializer.save(user=self.request.user)