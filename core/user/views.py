from rest_framework import generics, authentication, permissions
from rest_framework.settings import api_settings
from rest_framework.authtoken.views import ObtainAuthToken

from user.serializers import UserSerializer, TokenSerializer


class CreateUserView(generics.CreateAPIView):
    """ Criar novo usuário """
    serializer_class = UserSerializer


class CreateTokenView(ObtainAuthToken):
    """ Token para novos usuários """
    serializer_class = TokenSerializer
    renderer_classes = api_settings.DEFAULT_RENDERER_CLASSES


class ManageUserView(generics.RetrieveUpdateAPIView):
        """ Gerenciamento de usuário autenticado """
        serializer_class = UserSerializer
        authentication_classes = (authentication.TokenAuthentication,)
        permission_classes = (permissions.IsAuthenticated,)

        def get_object(self):
            """ Recuperar e retornar usuário autênticado """
            return self.request.user
