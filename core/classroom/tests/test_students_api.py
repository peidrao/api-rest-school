from django.contrib.auth import get_user_model
from django.urls import reverse
from django.test import TestCase

from rest_framework import status
from rest_framework.test import APIClient

from app.models import Student 
from classroom.serializers import StudentSerializer

STUDENTS_URL = reverse('classroom:student-list')

class PublicStudentApiTests(TestCase):
    """ Test the publicly available student's API """
    
    def setUp(self):
        self.client = APIClient()

    def test_login_required(self):
        """ Test that login is required for retrieving students """

        res = self.client.post(STUDENTS_URL)
        self.assertEqual(res.status_code, status.HTTP_401_UNAUTHORIZED)

class PrivateStudentApiTests(TestCase):
    """ Test the authorized user Students API """

    def setUp(self):
        self.user = get_user_model().objects.create_user(
            'peidra12321o@gmail.com',
            '@test1234'
        )

        self.client = APIClient()
        self.client.force_authenticate(self.user)
    
    def test_retrieve_students(self):
        """ Test retrieving students """
        Student.objects.create(user=self.user, name='Luiza', register=2018010920)
        Student.objects.create(user=self.user, name='Marizete', register=2018010920)

        res = self.client.get(STUDENTS_URL)

        student = Student.objects.all().order_by('-name')
        serializer = StudentSerializer(student, many=True)

        self.assertEqual(res.status_code, status.HTTP_200_OK)
        self.assertEqual(res.data, serializer.data)

    def test_students_limited_to_user(self):
        """ Test that args returned are for the authenticated """

        user2 = get_user_model().objects.create_user(
            'peidrao2@gmail.com',
            '@test1234'
        )

        Student.objects.create(user=user2, name='Pedro', register='123456')
        student = Student.objects.create(user=self.user, name='José', register='456789')

        res = self.client.get(STUDENTS_URL)

        self.assertEqual(res.status_code, status.HTTP_200_OK)
        self.assertEqual(len(res.data), 1)
        self.assertEqual(res.data[0]['name'], student.name)

    def test_create_student_successful(self):
        """ Test creating a new student """

        payload = {'name': 'José', 'register': '201801'}
        res = self.client.post(STUDENTS_URL, payload)

        exists = Student.objects.filter(
            user=self.user,
            name=payload['name']
        ).exists()

        self.assertTrue(exists)

    def test_create_student_invalid(self):
        """ Test creating a new student with invalid payload """
        payload = {'name': '', 'register': ''}
        res = self.client.post(STUDENTS_URL, payload)

        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)