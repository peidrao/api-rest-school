from django.contrib.auth import get_user_model
from django.urls import reverse
from django.test import TestCase

from rest_framework import status
from rest_framework.test import APIClient

from app.models import Teacher
from classroom.serializers import TeacherSerializer

TEACHERS_URL = reverse('classroom:teacher-list')

class PublicTeacherApiTests(TestCase):
    """ Test the publicly available teacher's API """

    def setUp(self):
        self.client = APIClient()

    def test_login_required(self):
        """ Test that login is required for retrieving teachers  """
        res = self.client.post(TEACHERS_URL)
        self.assertEqual(res.status_code, status.HTTP_401_UNAUTHORIZED)

class PrivateTeacherApiTests(TestCase):
    """ Test the authorized user teacher API """

    def setUp(self):
        self.user = get_user_model().objects.create_user(
            'fonseca@gmail.com', '@teste123354'
        )

        self.client = APIClient()
        self.client.force_authenticate(self.user)
    
    def test_retrieve_teachers(self):
        """ Test retrieving teachers """
        Teacher.objects.create(user=self.user, name='Maria')
        Teacher.objects.create(user=self.user, name='José')

        res = self.client.get(TEACHERS_URL)

        teacher = Teacher.objects.all().order_by('-name')
        serializer = TeacherSerializer(teacher, many=True)

        self.assertEqual(res.status_code, status.HTTP_200_OK)
        self.assertEqual(res.data, serializer.data)

    def test_teachers_limited_to_user(self):
        """ Test that args returned are for the authenticated """

        user2 = get_user_model().objects.create_user(
            'ribamar@gmail.com',
            '@ribamaaaaaar'
        )

        Teacher.objects.create(user=user2, name='Ribamar')
        teacher = Teacher.objects.create(user=self.user, name='Mateus')

        res = self.client.get(TEACHERS_URL)

        self.assertEqual(res.status_code, status.HTTP_200_OK)
        self.assertEqual(len(res.data), 1)
        self.assertEqual(res.data[0]['name'], teacher.name)

    def test_create_teacher_successful(self):
        """ Test creating a new teacher """

        payload = {'name': 'Irene'}

        res = self.client.post(TEACHERS_URL, payload)

        existx = Teacher.objects.filter(
            user=self.user,
            name=payload['name']
        ).exists()

        self.assertTrue(existx)

    def test_create_student_invalid(self):
        """ Test creating a new teacher with invalid payload """
        payload = {'name': ''}
        res = self.client.post(TEACHERS_URL, payload)

        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)