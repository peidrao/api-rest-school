from rest_framework import serializers

from app.models import Student, Teacher, ClassRoom

class StudentSerializer(serializers.ModelSerializer):
    """ Serializer for student objects """

    class Meta:
        model = Student
        fields = ('id', 'name', 'register')
        read_only_fields = ('id',)

class TeacherSerializer(serializers.ModelSerializer):
    """ Serializer for teacher objects """
    class Meta:
        model = Teacher
        fields = ('id', 'name', 'register')
        read_only_fields = ('id',)
    

class ClassRoomSerializer(serializers.ModelSerializer):
    """ classroom serializer """
    students = serializers.PrimaryKeyRelatedField(
        many=True, queryset=Student.objects.all())

    teachers = serializers.PrimaryKeyRelatedField(
        many=True, queryset=Teacher.objects.all())

    class Meta:
        model = ClassRoom
        fields = ('id', 'title', 'students', 'teachers')
        read_only_fields = ('id',)


class ClassRomDetailSerializer(ClassRoomSerializer):
    """ Serializer a classroom detail """
    students = StudentSerializer(many=True, read_only=True)
    teachers = TeacherSerializer(many=True, read_only=True)
