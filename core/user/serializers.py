from django.contrib.auth import get_user_model, authenticate
from django.utils.translation import gettext_lazy

from rest_framework import serializers


class UserSerializer(serializers.ModelSerializer):
    """ Serializer de usuários """


    class Meta:
        model = get_user_model()
        fields = ('email', 'password', 'name')
        extra_kwargs = {'password': {'write_only': True, 'min_length': 6, 'style': {'input_type': 'password'} }}

    def create(self, validation_data):
        """ Criar um novo usuário com senha encriptada """
        return get_user_model().objects.create_user(**validation_data)


class TokenSerializer(serializers.Serializer):
    """ Serializer para autênticação de novos usuários """

    email = serializers.CharField()
    password = serializers.CharField(
        style={'input_type': 'password'}, trim_whitespace=False)

    def validate(self, attrs):
        """ Validação de autênticação do usuário """

        email = attrs.get('email')
        password = attrs.get('password')

        user = authenticate(
            request=self.context.get('request'),
            username=email,
            password=password
        )

        if not user:
            msg = gettext_lazy('Não é possivel fazer autênticação com essas credênciais')
            raise serializers.ValidationError(msg, code='authorization')

        attrs['user'] = user
        return attrs
