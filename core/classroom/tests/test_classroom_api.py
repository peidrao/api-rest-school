from django.test import TestCase
from django.contrib.auth import get_user_model
from django.urls import reverse

from rest_framework import status
from rest_framework.test import APIClient

from app.models import ClassRoom, Teacher, Student

from classroom.serializers import ClassRoomSerializer, ClassRomDetailSerializer

CLASSROOMS_URL = reverse('classroom:classroom-list')

def detail_url(classroom_id):
    """ Return classroom detail URL """
    return reverse('classroom:classroom-detail', args=[classroom_id])

def sample_student(user, name='Luiz', register=125954):
    """ Create and return a sample student """
    return Student.objects.create(user=user, name=name, register=register)

def sample_teacher(user, name='Marcinho'):
    """ create and return a sample teacher """
    return Teacher.objects.create(user=user, name=name)

def sample_classroom(user, **params):
    """ Create and return a sample classroom """
    
    defaults = {
        'title': 'ABC'
    }

    defaults.update(params)
    return ClassRoom.objects.create(user=user, **defaults)


class PublicClassRoomApiTest(TestCase):
    """ Test unauthentication recipe API access """

    def setUp(self):
        self.client = APIClient()

    def test_auth_required(self):
        """ Test that authentication is required """
        res = self.client.get(CLASSROOMS_URL)
        self.assertEqual(res.status_code, status.HTTP_401_UNAUTHORIZED)


class PrivateClassRoomApiTests(TestCase):
    """ Test unauthentication classroom API access  """

    def setUp(self):
        self.client = APIClient()
        self.user = get_user_model().objects.create_user(
            'test@gmail.com',
            '@testpass123'
        )
        self.client.force_authenticate(self.user)

    def test_retrieve_classrooms(self):
        """ Test retrieving a list of classrooms """

        sample_classroom(user=self.user)
        # sample_classroom(user=self.user)

        res = self.client.get(CLASSROOMS_URL)

        classrooms = ClassRoom.objects.all().order_by('-id')
        serializer = ClassRoomSerializer(classrooms, many=True)

        self.assertEqual(res.status_code, status.HTTP_200_OK)
        self.assertEqual(res.data, serializer.data)

    def test_classrooms_limited_to_user(self):
        """ Test retrieving classrooms for user """
        user2 = get_user_model().objects.create_user(
        'user2@gmail.com',
        '@test123'
        )

        sample_classroom(user=user2)
        sample_classroom(user=self.user)

        res = self.client.get(CLASSROOMS_URL)
        classrooms = ClassRoom.objects.filter(user=self.user)
        serializer = ClassRoomSerializer(classrooms, many=True)

        self.assertEqual(res.status_code, status.HTTP_200_OK)
        self.assertEqual(len(res.data), 1)
        self.assertEqual(res.data, serializer.data)

    def test_view_classroom_detail(self):
        """ Test viewing a classroom detail """
        classroom = sample_classroom(user=self.user)
        classroom.teachers.add(sample_teacher(user=self.user))
        classroom.students.add(sample_student(user=self.user))

        url = detail_url(classroom.id)
        res = self.client.get(url)

        serializer = ClassRomDetailSerializer(classroom)

        self.assertEqual(res.data, serializer.data)
    
    def test_create_basic_classroom(self):
        """ Test creating classroom """
        payload = {'title': 'Basic Class'}

        res = self.client.post(CLASSROOMS_URL, payload)

        self.assertEqual(res.status_code, status.HTTP_201_CREATED)

        classroom = ClassRoom.objects.get(id=res.data['id'])

        for key in payload.keys():
            self.assertEqual(payload[key], getattr(classroom, key))

    def test_create_classroom_with_students(self):
        """ Test creating a classroom with students """

        student1 = sample_student(user=self.user, name='Josimar', register='20165456')
        student2 = sample_student(user=self.user, name='André', register='20147856')
        student3 = sample_student(user=self.user, name='Junin', register='20187856')
        payload = {
            'title': 'class1',
            'students': [student1.id, student2.id, student3.id]
        }
        
        res = self.client.post(CLASSROOMS_URL, payload)

        self.assertEqual(res.status_code, status.HTTP_201_CREATED)
        classroom = ClassRoom.objects.get(id=res.data['id'])

        students = classroom.students.all()
        self.assertEqual(students.count(), 3)
        self.assertIn(student1, students)
        self.assertIn(student2, students)
        self.assertIn(student3, students)

    def test_create_classroom_with_teachers_and_students(self):
        """ Test creating a classroom with students and teachers """

        student1 = sample_student(user=self.user, name='Pedro', register=1235045)
        student2 = sample_student(user=self.user, name='João', register=20184654)
        student3 = sample_student(user=self.user, name='Flora', register=20194954)

        teacher1 = sample_teacher(user=self.user, name='Maitão')

        payload = {
            'title': 'class2',
            'students': [student1.id, student2.id, student3.id],
            'teachers': [teacher1.id]
        }

        res = self.client.post(CLASSROOMS_URL, payload)
        self.assertEqual(res.status_code, status.HTTP_201_CREATED)

        classroom = ClassRoom.objects.get(id=res.data['id'])
        students = Student.objects.all()
        teachers = Teacher.objects.all()

        self.assertEqual(students.count(), 3)
        self.assertEqual(teachers.count(), 1)

        self.assertIn(student1, students)
        self.assertIn(student2, students)
        self.assertIn(student3, students)

        self.assertIn(teacher1, teachers)

         

    def test_create_classroom_with_teachers(self):
        """ Test creating a classroom with teachers """    
        teacher1 = sample_teacher(user=self.user, name='Marizete')
        teacher2 = sample_teacher(user=self.user, name='Edgar')
        teacher3 = sample_teacher(user=self.user, name='Antonino')

        payload = {
            'title': 'class2',
            'teachers': [teacher1.id, teacher2.id, teacher3.id]
        }

        res =self.client.post(CLASSROOMS_URL, payload)

        self.assertEqual(res.status_code, status.HTTP_201_CREATED)

        classroom = ClassRoom.objects.get(id=res.data['id'])

        teachers = classroom.teachers.all()
        self.assertEqual(teachers.count(), 3)

        self.assertIn(teacher1, teachers)
        self.assertIn(teacher2, teachers)
        self.assertIn(teacher3, teachers)

    def test_partial_update_classroom(self):
        """ Test updating a classroom with patch  """        

        classroom = sample_classroom(user=self.user)
        classroom.students.add(sample_student(user=self.user))
        new_student = sample_student(user=self.user, name='Maria', register=123546)

        payload = {
            'title': 'class3',
            'students': [new_student.id]
        }

        url = detail_url(classroom.id)
        self.client.patch(url, payload)

        classroom.refresh_from_db()
        self.assertEqual(classroom.title, payload['title'])
        students = classroom.students.all()
        self.assertEqual(len(students), 1)
        self.assertIn(new_student, students)
        
    def test_full_update_recipe(self):
        """ Test updating a recipe with a put """

        classroom = sample_classroom(user=self.user)
        classroom.students.add(sample_student(user=self.user))

        payload = { 'title': 'Class 3'}

        url = detail_url(classroom.id)
        self.client.put(url, payload)

        classroom.refresh_from_db()

        self.assertEqual(classroom.title, payload['title'])
        
        students = classroom.students.all()
        self.assertEqual(len(students), 0)