from django.urls import path, include
from rest_framework.routers import DefaultRouter

from classroom import views

router = DefaultRouter()
router.register('students', views.StudentViewSet)
router.register('teachers', views.TeacherViewSet)
router.register('classrooms', views.ClassRoomViewSet)

app_name = 'classroom'

urlpatterns = [
    path('', include(router.urls))
]
